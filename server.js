//This peer server
var ip = require('ip');
var express = require('express');
var express_peer_server = require('peer').ExpressPeerServer;
var peer_options = {
    debug: true,
    allow_discovery: true
};

var app = express();
var port = 9000;
var server = app.listen(port);

app.use('/peerjs', express_peer_server(server, peer_options));

console.log('peer server running on ' + ip.address() + ':' + port);

